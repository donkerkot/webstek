+++
title = "Wil je ons sponsoren?"
date = "2021-09-03"
+++

We zijn een zeer jonge organisatie, dus hebben we momenteel nog zeer beperkte middelen. We zijn dus op zoek naar sponsoring in allerlei vormen!

Zowel financieel, maar ook in natura. Denk maar aan audiovisueel materiaal bijvoorbeeld. Ook locatie-sponsoring zijn we gretig naar op zoek, zowel interessante ruimtes voor eenmalige en sporadische evenementen als een locatie om onze thuisbasis te noemen! 

Wat kunnen wij voor jou doen?
- Op livestreams, evenementen, deze website kunnen wij jouw logo zichtbaar afbeelden
- Jouw producten kunnen in beeld gebracht worden en gebruikt waar van toepassing
- Bij locatie-sponsoring trekken we jong volk aan naar jouw locatie

Heb jij interesse om ons verder te helpen op welke manier dan ook? 

Stuur ons een [mailtje](mailto:bestuur@donkerkot.org)!
