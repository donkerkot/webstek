+++
title = "Kozmotron @ Brouwerij Timmermans"
date = "2022-03-19T18:00:00+0100"
location = "Brouwerij Timmermans, Kerkstraat 11, 1701 Itterbeek"
description = "Live optreden"
price =  "€3"
draft = false
featuredImg = "https://f4.bcbits.com/img/a1968886277_10.jpg"
fburl = "https://www.facebook.com/events/2474973235971840"
+++
![banner](https://i.imgur.com/JL49zmB.png)

☾ DONKER KOT PRESENTEERT ☽

KOZMOTRON
{{< rawhtml >}}
<iframe style="border: 0; width: 100%; height: 120px;" src="https://bandcamp.com/EmbeddedPlayer/album=50261939/size=large/bgcol=ffffff/linkcol=0687f5/tracklist=false/artwork=small/transparent=true/" seamless><a href="https://kozmotron.bandcamp.com/album/qual">¿Qual? by KOZMOTRON</a></iframe>
{{< /rawhtml >}}

Psychedelic Space Rock.


Kom genieten van het live optreden van Kozmotron en geniet daarbij van het aanbod van Timmermans bieren op een unieke locatie aan de brouwerij zelf!

*TIMETABLE*

- 18u: Deuren
- 20u: Kozmotron
- 00u: Einde


[Kozmotron](https://www.facebook.com/Kozmotron)

[Timmermans](https://www.facebook.com/pages/Brouwerij-Timmermans/652191498246557)

[Event poster](https://i.imgur.com/AtP2OrQ.png)

{{< album 20220319-kozmotron >}}
