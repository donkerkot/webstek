+++
title = "UITGESTELD: Sara Della Santa"
date = "2021-12-11T20:00:00+0100"
location = "UITGESTELD"
description = "UITGESTELD"
price =  "Gratis!"
draft = true
featuredImg = "https://i.imgur.com/pcOEpIr.jpg"
fburl = "https://www.facebook.com/events/862069474479750"
+++

**OPGELET:**
Wegens een coronabesmetting in onze organisatie kan de livestream helaas niet door gaan. Maar uitstel is geen afstel, wij houden jullie verder op de hoogte.


[Sara op Instagram](https://www.instagram.com/sara.dellasantamusic/)

[Sara op Facebook](https://www.facebook.com/saraDSmusic)

[Sara op vi.be](https://vi.be/platform/saradsmusic)
