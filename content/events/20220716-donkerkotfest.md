+++
title = "Donker Kot Feest"
date = "2022-07-16T16:00:00+0100"
location = "Solleveld 11, Sint-Martens-Bodegem (Parking JK Vuilbak)"
description = "Mini-Festival"
price =  "€10 VVK, €12 ADK"
draft = false
fburl = "https://www.facebook.com/events/3224078464487531"
featuredImg = "/images/dkfeest2022-square.png"
ticketLink = "https://www.eventbrite.be/e/tickets-donker-kot-feest-2022-348778655347"
+++

Fan van wat zwaardere muziek of gewoon zin in een stevig feestje om de zomervakantie in te zetten? Zoek niet langer en zet 16 juli alvast in je agenda voor de eerste editie van Donker Kot Feest!

Time table:
- 16:00 [BOILR](https://www.facebook.com/boilrband) (Fuzz Rock)
- 17:15 [CAPITAN](https://www.facebook.com/capitanbandBE) (Post-Metal)
- 18:30 [CARDINAL](https://www.facebook.com/cardinalbandbe) (Hard Rock)
- 19:45 [DEADLINES](https://www.facebook.com/deadlinesmusic) (Metalcore)
- 21:00 [SCAVENGER](https://www.facebook.com/ScavengerOfficialBand) (Old-school Heavy Metal)
- 22:15 [WILDHEART](https://www.facebook.com/Wildheartrockandroll) (Glam Rock)
- 23:30 **AFTERPARTY MET TOUGH TITTY! (Coverband)**

[Poster](/images/dkfeest2022.png)

📹 **AFTERMOVIE** (Dieter Deneef)
{{< youtube NpAZjwmah7A >}}

📷 [Yne Pauwels](https://www.instagram.com/yp.snapshots/)
{{< album 20220716-donkerkotfeest-yp >}}
