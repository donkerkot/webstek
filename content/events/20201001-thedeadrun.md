+++
title = "The Dead Run"
date = "2021-10-01T20:00:00+0100"
location = "Online"
description = "Livestream"
price =  "Gratis!"
featuredImg = "https://f4.bcbits.com/img/a3124751780_16.jpg"
draft = false
fburl = "https://www.facebook.com/events/1579969382334529"
+++

Herbekijk deze livestream hier:
{{< youtube OemEtiC9_rI >}}

(Tijdens het eerste nummer waren er wat geluidsproblemen, maar naarmate de stream vordert is dit opgelost)

Om het kersvers initiatief Donker Kot te jumpstarten, en onze technische skills bij te schaven, livestreamen we onze technische demo in de vorm van een miniatuur optreden.
Muzikaal worden we bijgestaan door groot-Dilbeeks talent: The Dead Run.
Instrumentale Post-Metal met een serieuze groove aan, zeker de moeite om live (of livestreamed) eens te zien!
We voorzien de mogelijkheid om vragen aan de groep te stellen.

{{< rawhtml >}}
<iframe style="border: 0; width: 100%; height: 42px;" src="https://bandcamp.com/EmbeddedPlayer/album=4078151424/size=small/bgcol=ffffff/linkcol=0687f5/transparent=true/" seamless><a href="https://thedeadrun.bandcamp.com/album/session-i-recorded-live-in-puurs">SESSION I - Recorded Live in Puurs by The Dead Run</a></iframe>
{{< /rawhtml >}}

