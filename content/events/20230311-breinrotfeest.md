+++
title = "Breinrot Feest"
date = "2023-03-11T17:30:00+0100"
location = "JK Paddestoel, Alfons Gossetlaan 72, Groot Bijgaarden"
description = "Grindcore"
price =  "€10"
featuredImg = "/images/breinrot2023-square.png"
fburl = "https://www.facebook.com/events/1192469911369476"
+++

![Banner](/images/breinrot2023-banner.png)

**NL:**
Donker Kot nodigt jullie gaarne uit om samen te grinden op een avondje feestcore!
Slam, grind en groove op de deuntjes van jullie favoriete feestgroepen uit Vlaanderen en Nederland!
Als uw IQ niet met 15 punten gezakt zal zijn, geld terug! (cashback te vragen bij Koning Fluppe)

**ENG:**
Donker Kot welcomes y'alls to grind at a night of pure partycore!
Slame, grind & groove to the tunes of your favourite partybands that Flanders and the Netherlands have to offer!
If your IQ hasn't dropped by at least 15 points, money back guarantee! (ask King Fluppe for the cashback)

ATD: 10 euries
Presales: 10 euries + 2 free drinks

*TIMETABLE*
- 17:30 The Doors
- 18:00 [SPAUCH](https://www.facebook.com/SPAUCHURGH)
- 19:00 [BLUEWAFFLE SAUERKRAUT](https://www.facebook.com/BluewaffleGoregrind)
- 20:00 [STEENGRUIS](https://www.facebook.com/profile.php?id=100063705213530)
- 21:00 [VOLIERE](https://www.facebook.com/voliereslam)
- 22:00 [KAASSCHAAF](https://www.facebook.com/KaasschaafPartyGrind)


📹 **AFTERMOVIE** (Art en edit: [Ellen](https://www.instagram.com/aplenthy), Camera: [Goedele](https://www.instagram.com/_goedele))
{{< youtube P1e8TQt5p8I >}}


📷 [Yne Pauwels](https://www.instagram.com/yp.snapshots/)
{{< album 20230311-breinrot-yp >}}
