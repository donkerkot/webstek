+++
title = "Vijverfestival: Arena Podium"
date = "2022-07-09T18:00:00+0100"
location = "Kasteel Dilbeek"
description = "Outdoor podium"
price =  "Gratis!"
draft = false
featuredImg = "/images/vijverlogo.png"
+++

Op 9 juli zal Donker Kot het Arena podium hosten op Vijverfestival!

De line-up is als volgt:

**18u30: THURASON**

Waar Thurason komt, wordt intens geleefd en geliefkoosd. Een optreden van deze gitarist en zanger neemt je mee langs al het mooie wat het leven in petto heeft, al worden ook de hindernissen niet geschuwd. Met zijn mondharmonica als kers op de taart is het voor de luisteraar genieten van zijn mooie liedjes, zonder veel toeters en bellen maar met levenswijsheid voor een heel bestaan. Bart Peeters en Yevgueni achterna.

**19u10: SARA DELLA SANTA**

Melancholisch en bloedeerlijk, naar het voorbeeld van grote namen als Phoebe Bridgers en Strand of Oaks. Zo omschrijft Sara Della Santa haar geluid, waarin de Antwerpse singer-songwriter je onderdompelt in haar leven en hoe ook zijn wel een worstelt met alles wat om haar heen gebeurt.

**19u50: KAREN VAN PETEGHEM**

Tussen al het muzikale geweld is er ook plaats voor humor. Daarvoor tekent Karen Van Peteghem, die al menige comedy-avond opfleurde met haar aanwezigheid en met haar memes en video’s ook het internet op regelmatige basis vloert met spitsvondige humor. Dat wordt lachen!

**21u00: KAPITEIN MORGEN**

Rocken. Daarvoor zijn de jonge snaken van Kapitein Morgen op de planeet gezet. In het Nederlands brengen ze een eigen repertoire waarin liefde en het dagelijks bestaan een essentiële rol spelen. Neem het van ons aan: dit wordt een welkome dosis zuurstof tijdens een prachtige festivaldag.

📷 [Yne Pauwels](https://www.instagram.com/yp.snapshots/)
{{< album 20220709-vijverfestival-yp >}}
