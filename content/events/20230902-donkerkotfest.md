+++
title = "Donker Kot Feest 2023"
date = "2023-09-02T16:00:00+0100"
location = "JC Castelhof, Molenstraat 102, 1700 Dilbeek"
description = "Mini-Festival"
price =  "VVK: 10 Euro, Kassa: 15 Euro"
draft = false
featuredImg = "/images/dkotfeest2023-square.png"
+++

![banner](/images/donkerkotfeest2023-banner.png "Donker Kot Feest 2023 Banner")

Donker Kot Feest '23 @ JC Castelhof  
_The smooth sailin' Summer edition_  

Primal Creation
+
More bands TBA

Komt een komt allen komt vooral feesten en wat pintjes drinken!  
Dilbeek gaat weer zwaar, u komt toch ook?

**Tickets available soon!**

*TIMETABLE*
- **Doors** - 16h30
- **End** - 02h30

📹 **AFTERMOVIE VORIGE EDITIE** (Dieter Deneef)
{{< youtube NpAZjwmah7A >}}
