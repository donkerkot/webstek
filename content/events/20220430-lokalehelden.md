+++
title = "Lokale Helden Dilbeek"
date = "2022-04-30T19:00:00+0100"
location = "CC Westrand, Kamerijklaan 46, 1700 Dilbeek"
description = "Live optredens"
price =  "Gratis"
draft = false
fburl = "https://www.facebook.com/events/2195430953929746"
#featuredImg = "https://cds.stubru.be/sites/default/files/styles/1600x800/public/article/2017_04/schermafbeelding_2017-04-15_om_15.44.15.jpg?itok=zCONs3Nd"
featuredImg = "/images/lh2022.jpg"
+++

![banner](/images/lh2022-banner.jpg "Lokale Helden Banner")

Muzikale line-up met te ontdekken talent uit de buurt.

Deuren om 19 uur

20:00 ELES (Rap/Hiphop) uit Dilbeek

Lucas De Smet also known as ELES is een 20-jarige rapper uit Dilbeek.Hij geeft zijn gedachten, ervaringen, mening en humor bloot in zijn teksten. Je vindt zowel een rappende als zingende ELES terug in zijn nummers.


21:00 Moose (Hiphop/Rap) uit Dilbeek

Als Dilbeekse jonge leeuw schetst Moose zijn wereldvisie in doordachte barz. Met spitsvondige woordspelingen breidt hij flarden aan elkaar tot ze verhalen vormen. Met woorden als penseelstreken schildert hij een beeld of idee.


22:00 Thurason (Folk/Kleinkunst) uit Dilbeek

Thurason brengt Nederlandstalig muziek met zijn stem, gitaar, mondharmonica en teksten uit het leven gegrepen. Meer is er niet nodig.


23:00 The Dead Run (Post-Metal) uit Ninove/Schepdaal

Heavy, groovy, fast, slow, crushing, soothing and instrumental post-metal. 

📷 [Yne Pauwels](https://www.instagram.com/yp.snapshots/)
{{< album 20220430-lokalehelden-yp >}}

📷 Kenny VdM
{{< album 20220430-lokalehelden-kenny >}}
