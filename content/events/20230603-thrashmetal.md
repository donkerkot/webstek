+++
title = "Thrash n' Bash: Cobracide/Cuttermess"
date = "2023-06-03T18:00:00+0100"
location = "JK Roesj, Stationsstraat 275 1700 Dilbeek"
description = "Thrash/Death Metal"
price =  "10 Euro"
draft = false
featuredImg = "/images/cobracidecuttermess-banner.jpg"
fburl = "https://www.facebook.com/events/786059259608003"
+++

![Banner](/images/cobracidecuttermess-banner.jpg)

🌚 Donker Kot 🌚 presenteert:

Thrash n' Bash

Cobracide & Cuttermess @ JK Roesj

- The Doors - 18:00
- Axident - 18:45
- Cuttermess - 20:00
- Cobracide - 21:15
- Afterparty met DJ Dietrich & Zwos - 22:15 - 01:00

Come join us for a night of heavy partying!

If the banging tunes of Axident, Cobracide & Cuttermess leave you longing for more, fear not for DJ "Motherflippin Dietrich" and "Zwoele Zwos" will have you swinging and grooving and drinking until the bar's been emptied.

📷 [Yne Pauwels](https://www.instagram.com/yp.snapshots/)
{{< album 20230603-thrash-yp >}}
