+++
title = "Over Ons"
date = "2021-09-03"
+++

Donker Kot is een vereniging uit Dilbeek met een hart voor live muziek. Sinds jaar en dag zien wij het aantal live optredens in de streek slinken. 
Waar je vroeger elke week terecht kon in 1 van de vele jeugdklups in de streek om een of andere metal of punk band te gaan kijken, is dit tegenwoordig al een zeldzaam voorval aan het worden.

Ook de podiumkansen voor de beginnende bands zijn hierdoor sterk achteruit aan het gaan. Aan deze zaken hopen wij verandering te brengen!

Wij zijn een jonge, gefocuste vereniging met leden die gemotiveerd zijn, vaak een muzikale en/of technische achtergrond hebben, en ervaring in kleinschalige organisatie van live optredens onder de riem hebben.
We hopen onze ervaringen samen te leggen, en dit naar het volgende niveau te tillen.

Onze droom is om een vaste stek te hebben, waar we zowel laagdrempelig kunnen organiseren als een ontmoetingsfunctie voor jeugd kunnen belichamen. Zover zijn we helaas nog niet, dus voorlopig zul je ons kunnen vinden online en op verplaatsing in de verschillende zalen en klups in en rondom Dilbeek. Tot dan!
